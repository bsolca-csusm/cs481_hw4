﻿using System;
using ContactBookApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ContactBookApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactDetailPage : ContentPage
    {
        public event EventHandler<Contact> ContactAdded;
        public event EventHandler<Contact> ContactUpdated;

        public static readonly BindableProperty ContactProperty =
            BindableProperty.Create("EntryCell", typeof(Contact), typeof(ContactDetailPage));

        public Contact MyContact
        {
            get => (Contact) GetValue(ContactProperty);
            set => SetValue(ContactProperty, value);
        }

        public ContactDetailPage(Contact contact)
        {
            MyContact = contact;
            BindingContext = this;
            InitializeComponent();
            if (MyContact == null)
            {
                DisplayAlert("oui", "oui", "oui");
                return;
            }
            switch (MyContact.Gender)
            {
                case EGender.Female:
                    PickerGender.SelectedIndex = 0;
                    break;
                case EGender.Male:
                    PickerGender.SelectedIndex = 1;
                    break;
                default:
                    PickerGender.SelectedIndex = 2;
                    break;
            }
        }

        async void OnSave(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(MyContact.FirstName) ||
                String.IsNullOrWhiteSpace(MyContact.LastName)) 
            {
                await DisplayAlert("Error", "Please enter the name.", "OK");
                return;
            }
            switch (PickerGender.SelectedItem.ToString())
            {
                case "Male":
                    MyContact.Gender = EGender.Male;
                    MyContact.GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.male.png");
                    break;
                case "Female":
                    MyContact.Gender = EGender.Female;
                    MyContact.GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.female.png");
                    break;
                default:
                    MyContact.Gender = EGender.None;
                    MyContact.GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.robot.png");
                    break;
            }
            if (MyContact.Id == -1)
            {
                MyContact.Id = new Random().Next();
                ContactAdded?.Invoke(this, MyContact);
            }
            else
            {
                ContactUpdated?.Invoke(this, MyContact);
            }

            await Navigation.PopAsync();
        }
    }
}