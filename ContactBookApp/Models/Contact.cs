﻿using Xamarin.Forms;

namespace ContactBookApp.Models
{
    public enum EGender
    {
        None,
        Female,
        Male,
    }
    public class Contact
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool IsBlocked { get; set; }
        public EGender Gender { get; set; }
        public ImageSource GenderImageSource { get; set; }
        public string CompleteName => FirstName + " " + LastName;
    }
}
