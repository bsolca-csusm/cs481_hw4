﻿using System;
using System.Collections.ObjectModel;
using ContactBookApp.Models;
using ContactBookApp.Services;
using Xamarin.Forms;

namespace ContactBookApp
{
    public partial class MainPage : ContentPage
    {
        private readonly ContactService _service;
        private ObservableCollection<Contact> _contacts;
        public MainPage()
        {
            _service = new ContactService();
            _contacts = _service.GetContacts();
            InitializeComponent();
            ListView.ItemsSource = _contacts;
        }

        async void OnAddContact(object sender, EventArgs e)
        {
            var myContact = new Contact {Id = -1};
            var page = new ContactDetailPage(myContact);
            page.ContactAdded += (source, contact) =>
            {
                _service.AddContact(contact);
            };

            await Navigation.PushAsync(page);
        }

        private async void OnContactSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (ListView.SelectedItem == null)
                return; 

            var selectedContact = e.SelectedItem as Contact;
            ListView.SelectedItem = null;

            var page = new ContactDetailPage(selectedContact);
            page.ContactUpdated += (source, contact) =>
            {
                if (selectedContact == null) return;
                selectedContact.Id = contact.Id;
                selectedContact.FirstName = contact.FirstName;
                selectedContact.LastName = contact.LastName;
                selectedContact.Phone = contact.Phone;
                selectedContact.Email = contact.Email;
                selectedContact.IsBlocked = contact.IsBlocked;
                selectedContact.GenderImageSource = contact.GenderImageSource;
            };
            await Navigation.PushAsync(page);
        }

        private void DeleteContact(object sender, EventArgs e)
        {
            var menuItem = sender as MenuItem;
            var contact = menuItem.CommandParameter as Contact;
            _service.DeleteContact(contact.Id);
            _contacts = _service.GetContacts();
            ListView.ItemsSource = null;
            ListView.ItemsSource = _contacts;
        }

        private void HandleRefreshing(object sender, EventArgs e)
        {
            // INotifyCollectionChanged is not use to have a small refreshing demo
            _contacts = _service.GetContacts();
            ListView.ItemsSource = null; // Small hack to refresh the ListView without INotifyCollectionChanged
            ListView.ItemsSource = _contacts;
            ListView.EndRefresh();
        }
    }
}
