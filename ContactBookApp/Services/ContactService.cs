﻿using System.Collections.ObjectModel;
using System.Linq;
using ContactBookApp.Models;
using Xamarin.Forms;

namespace ContactBookApp.Services
{
    class ContactService
    {
        private readonly ObservableCollection<Contact> _contacts = new ObservableCollection<Contact>
        {
            new Contact
            {
                Id = 0,
                Email = "john.doe@example.com",
                FirstName = "John",
                IsBlocked = false,
                LastName = "Doe",
                Phone = "0651496613",
                Gender = EGender.Male,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.male.png"),
            },
            new Contact
            {
                Id = 1,
                Email = "alice@example.com",
                FirstName = "Alice",
                IsBlocked = false,
                LastName = "Mice",
                Phone = "0602020202",
                Gender = EGender.Female,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.female.png"),
            },
            new Contact
            {
                Id = 2,
                Email = "test@example.com",
                FirstName = "Benjamin",
                IsBlocked = true,
                LastName = "Button",
                Phone = "0602020202",
                Gender = EGender.None,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.robot.png"),
            },
            new Contact
            {
                Id = 3,
                Email = "alice@example.com",
                FirstName = "Delphine",
                IsBlocked = false,
                LastName = "Biro",
                Phone = "0602020202",
                Gender = EGender.Female,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.female.png"),
            },
            new Contact
            {
                Id = 4,
                Email = "alice@example.com",
                FirstName = "Mok",
                IsBlocked = false,
                LastName = "Muce",
                Phone = "0602020202",
                Gender = EGender.None,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.robot.png"),
            },
            new Contact
            {
                Id = 5,
                Email = "alice@example.com",
                FirstName = "Brice",
                IsBlocked = false,
                LastName = "De nice",
                Phone = "0602020202",
                Gender = EGender.Male,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.male.png"),
            },
            new Contact
            {
                Id = 6,
                Email = "alice@example.com",
                FirstName = "Leonardo",
                IsBlocked = false,
                LastName = "Da Vinci",
                Phone = "0602020202",
                Gender = EGender.None,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.robot.png"),
            },
            new Contact
            {
                Id = 7,
                Email = "alice@example.com",
                FirstName = "Pablo",
                IsBlocked = false,
                LastName = "Picasso",
                Phone = "0602020202",
                Gender = EGender.None,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.robot.png"),
            },
            new Contact
            {
                Id = 8,
                Email = "alice@example.com",
                FirstName = "George",
                IsBlocked = false,
                LastName = "Washington",
                Phone = "0602020202",
                Gender = EGender.Male,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.male.png"),
            },
            new Contact
            {
                Id = 9,
                Email = "alice@example.com",
                FirstName = "Mansa",
                IsBlocked = false,
                LastName = "Musa",
                Phone = "0602020202",
                Gender = EGender.None,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.robot.png"),
            },
            new Contact
            {
                Id = 10,
                Email = "alice@example.com",
                FirstName = "Richard",
                IsBlocked = false,
                LastName = "Nixon",
                Phone = "0602020202",
                Gender = EGender.Male,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.male.png"),
            },
            new Contact
            {
                Id = 11,
                Email = "alice@example.com",
                FirstName = "Wolfgang",
                IsBlocked = false,
                LastName = "Amadeus Mozart",
                Phone = "0602020202",
                Gender = EGender.None,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.robot.png"),
            },
            new Contact
            {
                Id = 12,
                Email = "alice@example.com",
                FirstName = "Pablo",
                IsBlocked = false,
                LastName = "Nerudo",
                Phone = "0602020202",
                Gender = EGender.Male,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.male.png"),
            },
            new Contact
            {
                Id = 13,
                Email = "alice@example.com",
                FirstName = "Napoleon",
                IsBlocked = false,
                LastName = "Bonaparte",
                Phone = "0602020202",
                Gender = EGender.Male,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.male.png"),
            },
            new Contact
            {
                Id = 14,
                Email = "alice@example.com",
                FirstName = "Maya",
                IsBlocked = false,
                LastName = "Angelou",
                Phone = "0602020202",
                Gender = EGender.Female,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.female.png"),
            },
            new Contact
            {
                Id = 15,
                Email = "alice@example.com",
                FirstName = "Marie",
                IsBlocked = false,
                LastName = "Cury",
                Phone = "0602020202",
                Gender = EGender.Female,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.female.png"),
            },
            new Contact
            {
                Id = 16,
                Email = "alice@example.com",
                FirstName = "Ludwig",
                IsBlocked = false,
                LastName = "Van Beethowen",
                Phone = "0602020202",
                Gender = EGender.None,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.robot.png"),
            },
            new Contact
            {
                Id = 17,
                Email = "alice@example.com",
                FirstName = "Mark",
                IsBlocked = false,
                LastName = "Twain",
                Phone = "0602020202",
                Gender = EGender.Female,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.female.png"),
            },
            new Contact
            {
                Id = 18,
                Email = "alice@example.com",
                FirstName = "Malcom",
                IsBlocked = false,
                LastName = "X",
                Phone = "0602020202",
                Gender = EGender.None,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.robot.png"),
            },
            new Contact
            {
                Id = 19,
                Email = "alice@example.com",
                FirstName = "Harry",
                IsBlocked = false,
                LastName = "Potter",
                Phone = "0602020202",
                Gender = EGender.Male,
                GenderImageSource = ImageSource.FromResource("ContactBookApp.Images.male.png"),
            },
        };

        public void AddContact(Contact newContact)
        {
            _contacts.Add(newContact);
        }

        public ObservableCollection<Contact> GetContacts() => _contacts;
        public Contact GetContactById(int id) => _contacts.Single(e => e.Id == id);

        public void EditContact(Contact editedContact)
        {
            var matchingContact = _contacts.FirstOrDefault(contact => contact.Id == editedContact.Id);
            if (matchingContact != null)
            {
                matchingContact = editedContact;
            }
        }

        public void DeleteContact(int id)
        {
            var itemToRemove = _contacts.FirstOrDefault(e => e.Id == id);
            _contacts.Remove(itemToRemove);
        }
    }
}